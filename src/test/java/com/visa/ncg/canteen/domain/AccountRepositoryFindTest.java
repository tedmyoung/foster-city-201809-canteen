package com.visa.ncg.canteen.domain;

import com.visa.ncg.canteen.FakeAccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositoryFindTest {
  @Test
  public void newRepositoryHasNoAccounts() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();

    assertThat(accountRepository.findAll())
        .isEmpty();

  }

  @Test
  public void findAllShouldReturn2Accounts() {
    Account account1 = new Account();
    account1.setId(1L);
    Account account2 = new Account();
    account2.setId(2L);

    AccountRepository repo = new FakeAccountRepository(account1, account2);
    assertThat(repo.findAll())
        .hasSize(2);
  }

  @Test
  public void findAllAfterSaving2AccountsReturns2Accounts() throws Exception {
    AccountRepository repo = new FakeAccountRepository();

    repo.save(new Account());
    repo.save(new Account());

    assertThat(repo.findAll())
        .hasSize(2);
  }

  @Test
  public void findOneForNonExistentIdReturnsNull() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();

    assertThat(accountRepository.findOne(999L))
        .isNull();
  }

  @Test
  public void findOneForExistingIdReturnsAccount() throws Exception {
    Account account1 = new Account();
    account1.setId(1L);
    AccountRepository accountRepository = new FakeAccountRepository(account1);

    assertThat(accountRepository.findOne(account1.getId()))
        .isNotNull();
  }

}