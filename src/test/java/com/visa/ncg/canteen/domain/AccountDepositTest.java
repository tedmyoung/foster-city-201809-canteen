package com.visa.ncg.canteen.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountDepositTest {

  @Test
  public void deposit10DollarsInNewAccountResultsInBalanceOf10Dollars() throws Exception {
    Account account = new Account();

    account.deposit(10);

    assertThat(account.balance())
        .isEqualTo(10);
  }

  @Test
  public void deposit7Then4DollarsResultsInBalanceOf11Dollars() throws Exception {
    Account account = new Account();

    account.deposit(7);
    account.deposit(4);

    assertThat(account.balance())
        .isEqualTo(11);
  }

  @Test
  public void depositNegativeAmountThrowsException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(-1);
    }).isInstanceOf(InvalidAmountException.class)
      .hasMessage("Amount was -1");

  }

  @Test
  public void depositZeroDollarsThrowsException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(0);
    }).isInstanceOf(InvalidAmountException.class)
      .hasMessage("Amount was 0");

  }
}