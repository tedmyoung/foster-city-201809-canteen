package com.visa.ncg.canteen.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountSetupTest {
  @Test
  public void newAccountHasZeroBalance() throws Exception {
    Account account = new Account();

    assertThat(account.balance())
        .isZero();
  }

  @Test
  public void newAccountWithInitialBalanceOf8DollarsResultsIn8DollarBalance() throws Exception {
    Account account = new Account(8);

    assertThat(account.balance())
        .isEqualTo(8);
  }

}