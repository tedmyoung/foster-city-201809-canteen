package com.visa.ncg.canteen.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountWithdrawTest {

  @Test
  public void withdraw3DollarsFromAccountHaving7DollarsResultsIn4DollarBalance() throws Exception {
    Account account = new Account(7);

    account.withdraw(3);

    assertThat(account.balance())
        .isEqualTo(4);
  }

  @Test
  public void withdraw6DollarsAndThen2DollarsFromAccountHaving11DollarsResultsIn3DollarBalance() throws Exception {
    Account account = new Account(11);

    account.withdraw(6);
    account.withdraw(2);

    assertThat(account.balance())
        .isEqualTo(3);
  }

  @Test
  public void withdrawNegativeAmountThrowsException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(-1);
    }).isInstanceOf(InvalidAmountException.class);

  }

  @Test
  public void withdrawZeroThrowsException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(0);
    }).isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdraw12FromAccountHavingBalance10ThrowsException() throws Exception {
    Account account = new Account(10);

    // Curly braces around the "account.withdraw" method call are optional because it's 1 line of code
    assertThatThrownBy(() -> account.withdraw(12))
        .isInstanceOf(InsufficientBalanceException.class);
  }

  @Test
  public void withdraw13FromAccountHavingBalance13ResultsInZeroBalance() throws Exception {
    Account account = new Account(13);

    account.withdraw(13);

    assertThat(account.balance())
        .isZero();

  }

}