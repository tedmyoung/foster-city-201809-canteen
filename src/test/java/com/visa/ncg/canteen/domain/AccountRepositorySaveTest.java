package com.visa.ncg.canteen.domain;

import com.visa.ncg.canteen.FakeAccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositorySaveTest {
  @Test
  public void saveForAccountWithExistingIdDoesNotChangeId() throws Exception {
    Account account2 = new Account(10);
    account2.setId(2L);

    AccountRepository accountRepository = new FakeAccountRepository();
    Account savedAccount2 = accountRepository.save(account2);

    assertThat(savedAccount2.getId())
        .isEqualTo(account2.getId());
  }

  @Test
  public void saveForNewAccountAssignsId() throws Exception {
    Account account = new Account(16);

    AccountRepository accountRepository = new FakeAccountRepository();
    Account savedAccount = accountRepository.save(account);

    assertThat(savedAccount.getId())
        .isNotNull();
  }

  @Test
  public void newlySavedAccountsHaveUniqueIds() {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account1 = new Account();
    account1 = accountRepository.save(account1);
    Account account2 = new Account();
    account2 = accountRepository.save(account2);

    assertThat(account1.getId())
        .isNotEqualTo(account2.getId());
  }
}