package com.visa.ncg.canteen.adapter.api;

import com.visa.ncg.canteen.FakeAccountRepository;
import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountApiControllerTest {
  @Test
  public void testGetMapping() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account(73);
    account.changeNameTo("Test");
    account.setId(123L);
    accountRepository.save(account);

    AccountApiController controller = new AccountApiController(accountRepository);

    AccountResponse accountResponse = controller.accountInfo("123");

    assertThat(accountResponse.getBalance())
        .isEqualTo(account.balance());
    assertThat(accountResponse.getName())
        .isEqualTo(account.name());
  }
}