package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class FakeAccountRepository implements AccountRepository {

  private Map<Long, Account> accountMap = new HashMap<>();
  private AtomicLong counter = new AtomicLong();

  @Autowired
  public FakeAccountRepository() {
  }

  public FakeAccountRepository(Account... accounts) {
    for (Account account : accounts) {
      save(account);
    }
  }

  public Account findOne(Long id) {
    return accountMap.get(id);
  }

  public Account save(Account entity) {
    ensureHasId(entity);
    accountMap.put(entity.getId(), entity);
    return entity;
  }

  private void ensureHasId(Account entity) {
    if (entity.getId() == null) {
      entity.setId(counter.getAndIncrement());
    }
  }

  public List<Account> findAll() {
    return new ArrayList<>(accountMap.values());
  }
}
