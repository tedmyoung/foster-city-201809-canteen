package com.visa.ncg.canteen.adapter.web;

import com.visa.ncg.canteen.domain.Account;

// DTO for web view of an account
public class AccountView {
  private int balance;
  private String name;
  private String id;

  public static AccountView transformFrom(Account account) {
    AccountView accountView = new AccountView();

    accountView.setBalance(account.balance());
    accountView.setName(account.name());
    accountView.setId(account.getId().toString());

    return accountView;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }
}
