package com.visa.ncg.canteen.adapter.api;

import com.visa.ncg.canteen.domain.Account;

public class AccountResponse {
  private long id;
  private int balance;
  private String name;

  public static AccountResponse transformFrom(Account account) {
    AccountResponse accountResponse = new AccountResponse();

    accountResponse.setId(account.getId());
    accountResponse.setBalance(account.balance());
    accountResponse.setName(account.name());

    return accountResponse;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }
}
