package com.visa.ncg.canteen.adapter.api;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountApiController {

  private final AccountRepository accountRepository;

  @Autowired
  public AccountApiController(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @GetMapping("/api/accounts/{id}")
  public AccountResponse accountInfo(@PathVariable("id") String accountId) {
    Long id = Long.parseLong(accountId);

    Account account = accountRepository.findOne(id);

    AccountResponse accountResponse = AccountResponse.transformFrom(account);

    return accountResponse;
  }

  @PostMapping("/api/accounts")
  public AccountResponse createAccount(@RequestBody AccountCreateRequest request) {
    Account account = new Account(request.getInitialBalance());
    account.changeNameTo(request.getAccountName());

    Account savedAccount = accountRepository.save(account);

    AccountResponse accountResponse = AccountResponse.transformFrom(savedAccount);

    return accountResponse;
  }

}
