package com.visa.ncg.canteen.adapter.web;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller // web view, not REST API
public class AccountWebController {

  private final AccountRepository accountRepository;

  @Autowired
  public AccountWebController(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @GetMapping("/account/{id}")
  public String accountView(@PathVariable("id") String accountId, Model model) {
    Long id = Long.parseLong(accountId);

    Account foundAccount = accountRepository.findOne(id);
    if (foundAccount == null) {
      throw new NoSuchAccountException();
    }

    AccountView accountView = AccountView.transformFrom(foundAccount);

    model.addAttribute("account", accountView);

    return "account-view";
  }

  @GetMapping("/account")
  public String allAccountView(Model model) {
    // more concise using method lambda
    List<AccountView> views =
        accountRepository.findAll()
                         .stream()
                         .map(AccountView::transformFrom)
                         .collect(Collectors.toList());


    model.addAttribute("accounts", views);

    return "all-accounts";
  }

  @GetMapping("/create-account")
  public String createAccountForm(Model model) {
    CreateForm createForm = new CreateForm();
    createForm.setAccountName("");
    createForm.setInitialDeposit(100);

    model.addAttribute("createForm", createForm);

    return "create-account";
  }

  @PostMapping("/create-account")
  public String createAccount(@ModelAttribute("createForm") CreateForm createForm) {
    Account account = new Account(createForm.getInitialDeposit());
    account.changeNameTo(createForm.getAccountName());

    Account savedAccount = accountRepository.save(account);

    return "redirect:/account/" + savedAccount.getId();
  }

}
