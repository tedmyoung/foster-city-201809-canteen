package com.visa.ncg.canteen.adapter.web;

// DTO for form handling
public class CreateForm {
  private int initialDeposit;
  private String accountName;

  public int getInitialDeposit() {
    return initialDeposit;
  }

  public void setInitialDeposit(int initialDeposit) {
    this.initialDeposit = initialDeposit;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }
}
