package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AccountDataLoader implements ApplicationRunner {
  private AccountRepository accountRepository;

  @Autowired
  public AccountDataLoader(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    // create a couple of accounts with initial balance and save them to the repository
    Account account1 = new Account(10);
    account1.changeNameTo("Luxuries");
    accountRepository.save(account1);
    Account account2 = new Account(20);
    account2.changeNameTo("Necessities");
    accountRepository.save(account2);
  }
}