package com.visa.ncg.canteen.domain;

import java.util.List;

public interface AccountRepository {
  Account findOne(Long id);

  Account save(Account entity);

  List<Account> findAll();
}
