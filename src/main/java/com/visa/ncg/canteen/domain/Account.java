package com.visa.ncg.canteen.domain;

public class Account {
  private int balance = 0;
  private Long id = null;
  private String name;

  public Account() {
  }

  public Account(int initialBalance) {
    balance = initialBalance;
  }

  public String name() {
    return name;
  }

  public void changeNameTo(String newName) {
    name = newName;
  }

  public int balance() {
    return balance;
  }

  public void deposit(int amount) {
    validateAmount(amount);
    balance += amount;
  }

  public void withdraw(int amount) {
    validateAmount(amount);
    validateSufficientBalance(amount);
    balance -= amount;
  }

  private void validateSufficientBalance(int amount) {
    if (amount > balance) {
      throw new InsufficientBalanceException("Could not withdraw " + amount);
    }
  }

  private void validateAmount(int amount) {
    if (amount <= 0) {
      throw new InvalidAmountException("Amount was " + amount);
    }
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
